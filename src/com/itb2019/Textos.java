package com.itb2019;

public class Textos {

    public Textos() {
    }
    // TODO: 18/11/2019 Buscar tots els usos de diesLaborals2XX, per utilitzar-hi també Constants.ESPAI
    private String diesLaborals1CA = "La setmana te ";
    private String diesLaborals2CA = "dies laborals.";

    private String diesLaborals1ES = "La semana tiene ";
    private String diesLaborals2ES = "dias laborales.";

    private String diesLaborals1EN = "The week has ";
    private String diesLaborals2EN = "working days.";

    private String no_controlat = "[!] Idioma no controlat";

    private String no_arguments = "No s'han indicat arguments";
    // TODO: 25/11/19 generar el getter


    public String getDiesLaborals1CA() {
        return this.diesLaborals1CA;
    }

    public String getDiesLaborals2CA() {
        return this.diesLaborals2CA;
    }

    public String getDiesLaborals1ES() {
        return this.diesLaborals1ES;
    }

    public String getDiesLaborals2ES() {
        return this.diesLaborals2ES;
    }

    public String getDiesLaborals1EN() {
        return this.diesLaborals1EN;
    }

    public String getDiesLaborals2EN() {
        return this.diesLaborals2EN;
    }

    public String fraseDiesLaborals(String idioma, int diesLaborals) {
        // TODO: 18/11/2019 modificar el nom de varialbe NO_CONTROLAT -> IDIOMA_NO_CONTROLAT;
        String frase = no_controlat;

        switch (idioma) {
            case Constants.ESPANYOL:
                frase = diesLaborals1ES + diesLaborals + diesLaborals2ES;
                break;
            // TODO: 18/11/2019 Afegir control per altres idiomes CA i EN
            default:
                break;
        }
        return frase;

    }
}
