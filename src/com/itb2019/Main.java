package com.itb2019;

import java.util.Arrays;

public class Main {

    // TODO: 18/11/2019 canviar DWEEK a DIES_SETMANA
    static final int DWEEK = 7;
    // TODO: 18/11/2019 Canviar DLABORABLES a DIES_LABORABLES
    static final int DLABORABLES = 5;
    static Textos textos;


    public static void main(String[] args) {
// write your code here
        System.out.println("args = " + Arrays.deepToString(args));
        System.out.println("DSETMANA = " + DWEEK);
        System.out.println("DIES_LABORABLES = " + DLABORABLES);
        System.out.println("-----------------------------------");

        textos = new Textos();

        // TODO: 25/11/19 Afegir control: si no hi ha arguments (args)
        //  mostrar el text de la variable "no_arguments" de la classe Textos i s'acabi l'execució


        for (int i = 0; i < args.length; i++) {
            System.out.println("\n* " + args[i] + ":");
            // TODO: 18/11/2019 Controlar els idiomes ESPANYOL i INGLES
            switch (args[i]) {
                case Constants.CATALA:
                    System.out.println(textos.getDiesLaborals1CA() + DLABORABLES + textos.getDiesLaborals2CA());
                    break;
                default:
                    // TODO: 25/11/19 Mostrar la variable NO_CONTROLAT 
                    System.out.println("[!] Idioma no reconegut");
            }
        }

        System.out.println("\n--------- UNA ALTRA OPCIÓ: ---------");

        for (int i = 0; i < args.length; i++) {
            System.out.println("\n* " + args[i] + ":");
            System.out.println(textos.fraseDiesLaborals(args[i], DLABORABLES));
        }
    }

}
